"""
Sort movies by popularity with Hadoop
"""
from mrjob.job import MRJob
from mrjob.step import MRStep


class MoviesBreakDown(MRJob):

    def steps(self):
        """
        It tells the framework what functions are used for mappers and
        reducers in our jobs.
        :return:
        """
        return [
            MRStep(mapper=self.mapper_get_movies,
                   reducer=self.reducer_count_movies),
            MRStep(reducer=self.reducer_get_sorted_output)
        ]

    def mapper_get_movies(self, _, line):
        """
        Mappers receive 3 parameters in Python using the MR job package.
        We want to extract the key of the movie and a value 1.

        :param self:    object instance that this is contained within
        :param _:       usually unused in the case of a mapper
                        BUT if you're chaining mapreduce stages together, this
                        might be a key coming in from a reducer that came
                        before you.
        :param line:    each input line of data that's coming in
        :return:        movie and the number one as the key-value pair, for
                        each input line (passed into the MapReduce framework)
        """
        (userID, movieID, rating, timestamp) = line.split('\t')
        yield movieID, 1

    def reducer_count_movies(self, key, values):
        """
        By default, streaming treats all input and output as strings. So
        things get sorted as strings, not numerically. Zero-pad our numbers
        so they'll sort properly.

        :param key:     our reducer function will be called once, for each
                        unique key, e.g. movie "Sherlock Holmes"
        :param values:  a list of all of the values associated with that key
                        (iterator)
        :return:        set of rating counts and movie IDs for each unique
                        movie ID
        """
        yield str(sum(values)).zfill(5), key

    def reducer_get_sorted_output(self, count, movies):
        """
        Just by virtue of sending the result through a second reducer we can
        end up sorting things by that first value of the output (as it will be
        sent through the sort and shuffle phase).
        """
        for movie in movies:
            yield movie, count


if __name__ == '__main__':
    MoviesBreakDown.run()
