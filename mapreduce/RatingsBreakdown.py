"""
MR job script in Python using MapReduce streaming
"""
from mrjob.job import MRJob
from mrjob.step import MRStep


class RatingsBreakDown(MRJob):

    def steps(self):
        """
        It tells the framework what functions are used for mappers and
        reducers in our jobs.
        :return:
        """
        return [
            MRStep(mapper=self.mapper_get_ratings,
                   reducer=self.reducer_count_ratings)
        ]

    def mapper_get_ratings(self, _, line):
        """
        Mappers receive 3 parameters in Python using the MR job package.
        We want to extract the key of the rating and a value 1.

        :param self:    object instance that this is contained within
        :param _:       usually unused in the case of a mapper
                        BUT if you're chaining mapreduce stages together, this
                        might be a key coming in from a reducer that came
                        before you.
        :param line:    each input line of data that's coming in
        :return:        rating and the number one as the key-value pair, for
                        each input line (passed into the MapReduce framework)
        """
        (userID, movieID, rating, timestamp) = line.split('\t')
        yield rating, 1

    def reducer_count_ratings(self, key, values):
        """
        :param key:     our reducer function will be called once, for each
                        unique key, e.g. rating 2
        :param values:  a list of all of the values associated with that key
                        (iterator)
        :return:        the key, which is going to be a rating from 1 to 5,
                        and the sum of all the values associated with that key
        """
        yield key, sum(values)


if __name__ == '__main__':
    RatingsBreakDown.run()
