
![Python](img/python_logo_2.jpg)

## Installing Python, MRJob and nano
#### to run our MapReduce job in our Hadoop installation

- ### HDP 2.6.5
  - coming soon

- ### HDP 2.5

  `su root`
  
  - #### PIP
    - cd /etc/yum.repos.d
    - cp sandbox.repo /tmp
    - rm sandbox.repo
    - cd ~
    - yum install python-pip
    
  - #### MRJob
    - pip install google-api-python-client==1.6.4
    - pip install mrjob==0.5.11
    
  - #### Nano
    - yum install nano
    
  - #### Data files
    - wget http://media.sundog-soft.com/hadoop/ml-100k/u.data
    - wget http://media.sundog-soft.com/hadoop/RatingsBreakdown.py

#### 
## Running with MRJob

   - #### Run locally
     `python RatingsBreakdown.py u.data`
     
   - #### Run with Hadoop
     `python RatingsBreakdown.py -r hadoop --hadoop-streaming-jar /usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar u.data`
     
     where
     
     - `--hadoop-streaming-jar`: MRJob doesn't really know where to find the jar file for Hadoop streaming on the Hadoop sandbox - we have to manually tell it where it is
     
####
## Challenge
 
Sort movies by popularity with Hadoop.
 
My [solution](TopMovies.py) can be found here.
 
![Solution](img/screenshot.jpeg)