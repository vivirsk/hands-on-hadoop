#!/bin/python3
"""Getting streaming data from Kafka with Spark Streaming with Python"""

import findspark
import argparse
import pyspark

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
import json
from kafka import KafkaProducer


producer = KafkaProducer(bootstrap_servers='localhost:9092',
                         value_serializer=lambda v:
                         json.dumps(v).encode('utf-8'))

import logging
logging.basicConfig(filename='/tmp/log/kafka_app.log', level=logging.INFO)
logger = logging.getLogger(__name__)


def check_data_quality(line):
    """
    Checks data quality and prints out error message in case of:
        - wrong data type and
        - missing data

    :param line:    line
    :return:        boolean representing whether data is in the right format
    """
    line = line.split(",")

    if len(line) == 4:
        try:
            id = int(line[0])
            age = int(line[3])
            return True
        except ValueError:
            logging.error("ERROR: Wrong data type entered")
        return False
    else:
        logging.error("ERROR: Missing field")
        return False


class FromKafka(object):
    """
    Class for the creation of objects streamed from Kafka.
    """
    def __init__(self, *params):
        """
        Creates objects streamed from Kafka with Spark Streaming.

        :param params:  It is used to send a non-keyworded variable length
                        argument list by initialisation.
                        It can be useful if the number of arguments streamed
                        can change.
        """
        self.id = params[0]
        self.name = params[1]
        self.country = params[2]
        self.age = int(params[3])


def parse_line(line):
    """
    Splits line into list at comma positions.
    Extracts and typecasts relevant fields - ID, Name, Country, Age

    Includes two solutions:
        - solution 1:   Simple solution for the current use case.
        - solution 2:   If the structure of the lines streamed will change
                        and we would like to maintain the code in one place
                        - FromKafka class.

    :return:    dict object
    """
    # solution 1
    # params = ["id", "name", "country", "age"]

    fields = [field.strip() for field in line.split(",")]

    # solution 2
    k = FromKafka(*fields)

    # return dict(zip(params, fields))
    return k.__dict__


def write_to_new_kafka_topic(message, topic):
    """
    Writes key-value data to a specific Kafka topic specified as a command
    line option.

    :param message:     message
    :param topic:       Kafka topic for output data
    """
    records = message.collect()

    for record in records:
        logger.info("Result: {}".format(json.dumps(record)))
        producer.send(topic, record)
        producer.flush()


def main():
    """
    Ingests streams of data from given Apache Kafka topic, applies various
    transformations and writes back to a different Kafka topic using Spark
    Streaming.

    Message processing:

        - step 1:
          Filters out poor-quality data (missing or not appropriate data)

        - step 2:
          Applies filtering:
          facoltativo:      age > 18
          predefined:       by default removes 'country' field, but it can be
                            a given field through the command line, too
    """
    parser = argparse.ArgumentParser("App for streaming data from Kafka with "
                                     "Spark Streaming with Python")
    parser.add_argument("brokers",
                        help="Kafka cluster consisting of one or more servers "
                             "- running it locally: localhost:9092")
    parser.add_argument("topic",
                        help="Kafka topic to stream data from.")
    parser.add_argument("-f", "--filter",
                        type=str,
                        default="country",
                        help="Filter out given field from output.\n"
                             "Default is 'country'.")
    parser.add_argument("--output-topic",
                        type=str,
                        default="kafka-output",
                        help="Kafka topic for output data.\n"
                             "Default is 'kafka-output'.")

    args = parser.parse_args()

    # Creates Spark context
    sc = SparkContext(appName="PythonStreamingDirectKafka")
    sc.setLogLevel("WARN")

    # Passes the Spark context along with the batch duration (2 seconds)
    ssc = StreamingContext(sc, 2)
    brokers, topic = args.brokers, args.topic

    # Ingests data from Kafka
    logger.info("Start data ingesting from Kafka")
    kvs = KafkaUtils.createDirectStream(ssc,
                                        [topic],
                                        {"metadata.broker.list": brokers})

    # Data processing
    logger.info("Start data processing...")
    lines = kvs.map(lambda x: x[1])
    new = lines.filter(check_data_quality)  # step 1

    if new:  # step 2
        parsed = new.map(parse_line) \
            .filter(lambda x: int(x['age']) > 18)\
            .map(lambda x: {k: v for k, v in x.items() if k != args.filter})

        parsed.pprint()
        parsed.foreachRDD(lambda f: write_to_new_kafka_topic(
            f, topic=args.output_topic))

    ssc.start()  # start the streaming context
    ssc.awaitTermination()


if __name__ == "__main__":
    main()
