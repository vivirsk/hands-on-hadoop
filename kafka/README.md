# Getting streaming data from Kafka with Spark Streaming with Python


## Task

- [x] Create a topic (input) and send some messages to it, which message is separated by comma, its ordering: ID, Name, Country, Age (e.g. 1234, John Smith, USA, 42)
- [x] Implement in a chosen language: **Python**
- [x] Read the messages
- [x] Parse the messages
- [x] Implement a facoltativo filtering (e.g. Age > 18)
- [x] Write back the filtered data converted to JSON to another (output) topic without Country - meaning containing only ID, NAME, AGE fields
- [x] Print content of the output topic to standard output

## Here I am using

**System** 

* Kafka - version 2.0.0
* Spark - version 2.1.1

* Java version - 1.8.0_181 (Oracle Corporation)
* Scala version - version 2.11.8
* Python version - 3.5.2

**Libraries**

* findspark - 1.3.0
* pyspark - 2.1.2
* kafka - 1.3.5

## Workflow


### Initial steps
###
`cd kafka_2.11-2.0.0`

* Start Zookeeper

`export CLASSPATH="$CLASSPATH":"/opt/kafka_2.12-1.0.0/src/main/"`

`bin/zookeeper-server-start.sh config/zookeeper.properties`

* Start Kafka

`bin/kafka-server-start.sh config/server.properties`

* Start producer

`bin/kafka-console-producer.sh --broker-list localhost:9092 --topic <topic>`

where
- `topic` - Kafka topic name for streaming - e.g. _vivi-kafka_
- `output-topic` - another Kafka topic name for output data - e.g. _vivi-kafka-output_

* Start consumer

`bin/kafka-console-consumer.sh --bootstrap-server <server> --topic <topic> --from-beginning`

- `server` - in my case _localhost:9092_

* Start consumer for output topic

`bin/kafka-console-consumer.sh --bootstrap-server <server> --topic <output-topic> --from-beginning`


#
### Start the app

`cd spark-2.1.1-bin-hadoop2.7`

`export PYSPARK_PYTHON=/usr/bin/python3`

`bin/spark-submit --jars <jars_path> /home/<user>/hands-on-hadoop/kafka_task.py <broker> <topic> --filter country --output-topic <output-topic>`

where

- `jars_path` - Path of jars. You can download it from   - e.g. /home/<user>/Downloads/spark-streaming-kafka-0-8-assembly_2.11-2.1.1.jar
- `broker` - Kafka cluster consisting of one or more servers. Here I am running it locally - _localhost:9092_
- `topic` - Kafka topic to stream data from - e.g. _vivi-kafka_

optional:

- `--filter` - Filters out the given field from the output - by default _country_. e.g. _--filter age_
- `--output-topic` - Kafka topic for output data - by default _kafka-output_. e.g. _--output-topic vivi-kafka-output_

### Input data to producer

* 1234, John Smith, USA, 42
* 1235, Harry Potter, UK, 29
* 1236, Vivien Ruska, HU, 28
* 1237, Emily Smith, UK, 16
* 1238, Roland Kiss, HU, 33

or see: ![Input topic](img/kafka_topic_input.jpeg)

<br>
The app will filter out poor-quality data:

* 1239, Vivien Ruska, HU, 28e
* 1239, Vivien Ruska, HU, 28.0

will output: `ERROR: Wrong data type entered` message

* 1239, Vivien Ruska, HU

will output: `ERROR: Missing field` message

like this: ![Error in data](img/data_error_msg.jpeg)

### Output data on consumer

* The app transforms data as it comes in
  * filters out data where _age_ <= 18
  * removes country or chosen field 
  * converts output data to JSON and sends it back to another (chosen or default) Kafka topic

![Output topic](img/streaming_output_to_new_kafka_topic.jpeg)

## Future enhancements

* unit & integration tests