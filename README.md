![Hadoop](img/hadoop2.jpg)

# Installation

## 1. Install Hadoop stack (on your PC)

- [ ] Install a virtual machine on your desktop - which will let us run a virtual Linux box on our PC
- [ ] image for Hadoop (from Hortonworks): hortonworks.com/products/sandbox/
- [ ] download the sandbox (HDP 2.5 on Hortonworks Sandbox for virtualbox) - which is a pre-installed Hadoop environment
- [ ] open up that ova file that we downloaded --> click the Import button to import that image into virtualbox

## 2. Install Hadoop stack (on AWS)

SSH to machine: `ssh -i ./SSH/<keyname> ec2-user@<public-DNS-of-the-machine>`

`sudo yum update -y`

`sudo yum install yum-utils`

`sudo yum-config-manager --enable rhui-REGION-rhel-server-extras`

`sudo yum install docker -y`

`sudo sed -i.backup 's/\(^OPTIONS=.*\)"$/\1 --storage-opt=dm.basesize=20G"/' /etc/sysconfig/docker`

`sudo service docker start`

`sudo usermod -a -G docker ec2-user`

`sudo service docker status`

`sudo docker info`

`curl -O http://hortonassets.s3.amazonaws.com/2.5/HDP_2.5_docker.tar.gz`

`docker load -i HDP_2.5_docker.tar.gz`

`docker images`

`curl -O https://gist.githubusercontent.com/orendain/8d05c5ac0eecf226a6fed24a79e5d71a/raw/041ddbe52687570923bb51c5fe988c88cba94d64/start-sandbox-hdp-25.sh`

`sudo bash start-sandbox-hdp-25.sh`

###### # copy file to root
`sudo cp start-sandbox-hdp-25.sh /root/.`

###### # echo "bash /root/start_sandbox.sh" >> /etc/rc.local (permission denied)
###### # copy manually
`sudo vim /etc/rc.local`

###### # edit security groups (for inbound on port 8888 and 8080)
`echo -e "##\nAccess the Sandbox at:\nhttp://$(curl -sS4 icanhazip.com):8888\n##"`

`ssh -p 2222 maria_dev@localhost`
###### # pwd: maria_dev

## Data

source: grouplens.org

- user ID
- movie ID
- rating (score 1-5)
- timestamp
